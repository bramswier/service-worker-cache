// https://developers.google.com/web/ilt/pwa/caching-files-with-service-worker

const cacheName = 'service-worker-cache';
const cacheAssets = [
	'/static/css/all.css',
	'/static/js/main.js'
];
const ignoreSearch = true;

// Cache specified assets on a fresh install.
self.addEventListener('install', event => {
	event.waitUntil(
        caches.open(cacheName)
            .then(cache => cache.addAll(cacheAssets))
    );
});

// Intercept requests, retrieve matched files from cache and cache new version if needed.
self.addEventListener('fetch', event => {
	// Debugging
	caches.match(event.request, { cacheName })
       .then(response => response && console.log('match', response, event.request.url));

   	// Match the requested filename without a parameter with filenames in the cache, also without parameters.
   	// Example: all.css?v=1 will match all.css?v=2, since "?v=" will be ignored.
	event.respondWith(
        caches.match(event.request, { cacheName, ignoreSearch })
            .then(response => {
            	// The requested file is not in the cache. Retrieve it via a network request.
				if (!response) return fetch(event.request);

                // The requested file matches the cached file exactly (including parameter), return cached file.
				if (event.request.url === response.url) return response;

				// The filename matches, but the parameters don't. Retrieve new version of the requested file via a network request.
				return Promise.resolve(getAndCacheFile(event, caches));
			})
    );
});

function getAndCacheFile(event, caches) {
	const fetchRequest = event.request.clone();
	return fetch(fetchRequest)
		.then(fetchResponse => {
			const responseToCache = fetchResponse.clone();

			caches.open(cacheName)
				.then(cache => {
					// Remove old version of cached file by ignoring the parameter.
					cache.delete(event.request, { cacheName, ignoreSearch })
						.then(() => {
							// Cache new version of the file, with parameter.
							cache.put(event.request, responseToCache);
						});
					})

					return fetchResponse;
		})
};

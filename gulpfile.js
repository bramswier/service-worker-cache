const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const browserSyncReload = browserSync.reload;
const del = require('del');
const runSequence = require('run-sequence');
const gulpSourcemaps = require('gulp-sourcemaps');
const browserify = require('browserify');
const vinylSourceStream = require('vinyl-source-stream');
const vinylBuffer = require('vinyl-buffer');
const babelify = require('babelify');
const gulpUglify = require('gulp-uglify');

gulp.task('clean', () => {
	return del.sync('dist');
});

gulp.task('css-copy', () => {
	return gulp.src('./src/static/css/all.css')
		.pipe(gulp.dest('./dist/static/css/'))
});

gulp.task('html-copy', () => {
	return gulp.src('./src/index.html')
		.pipe(gulp.dest('./dist/'))
});

gulp.task('js-main-compile', () => {
	const bundler = browserify({
		entries: './src/static/js/main.js',
		debug: true
	});

	return bundler
		.transform(babelify)
		.bundle()
		.pipe(vinylSourceStream('main.js'))
		.pipe(vinylBuffer())
		.pipe(gulpSourcemaps.init({ loadMaps: true }))
		.pipe(gulpUglify())
		.pipe(gulpSourcemaps.write('.'))
		.pipe(gulp.dest('./dist/static/js/'))
});

gulp.task('js-service-worker-compile', () => {
	browserify('./src/service-worker.js')
		.transform(babelify)
		.bundle()
		.pipe(vinylSourceStream('service-worker.js'))
		.pipe(gulp.dest('./dist/'));
});

gulp.task('browsersync', () => {
	browserSync.init({
		server: {
			baseDir: 'dist'
		},
		notify: false,
		open: false
	});
});

gulp.task('watch', () => {
	gulp.watch('./src/static/css/all.css', ['css-copy']);
	gulp.watch('./src/index.html', ['html-copy']);
	gulp.watch('./src/static/js/main.js', ['js-main-compile']);
	gulp.watch('./src/service-worker.js', ['js-service-worker-compile']);
});

gulp.task('dev', cb => {
	runSequence('clean', 'css-copy', 'js-main-compile', 'js-service-worker-compile', 'html-copy', 'browsersync', 'watch', cb);
});

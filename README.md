# How-to
* Install: `yarn` / `npm i`.
* Develop: `gulp dev`.

# About
This is a format for caching and updating static assets (CSS, JavaScript) with a Service Worker, based on the presence of a query string. This could be done on the back-end side: each time there is a new build, this query string should be different. Instructions to test it are displayed when you view the page. 
